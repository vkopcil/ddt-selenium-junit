## Testing Web Application the modern way

Purpose of this project is to demonstrate combination of stable frameworks with modern test approach.

### Used frameworks

* Selenium - de-facto industry standard for handling web browser
* Java - as Selenium itself is written in it, it's logical choice
* JUnit - again, standard for test execution, with many interesting features
* Maven - the standard build framework in Java world
* [Allure](https://github.com/allure-framework/allure-core) - to generate beautiful reports with screenshots attached

### Test approach

Everything is designed as data driven, using JUnit ability for parameterized execution.
This way, every single test can be debugged easily, but they still can be executed in parallel using standard Surefire plugin etc.

As the most consuming operation during web testing is starting the browser itself, we re-using browser in same thread with necessary care about closing extra windows or navigation to proper URL on beginning of the test.

According test structure, we following Page Object and Page Factory design pattern.
For better readability we might use page fragments, unfortunately they aren't directly supported by Selenium.

### Supported drivers

For local execution only FireFox is currently supported. That's for simplicity, because it's only browser which doesn't require Selenium driver. It might support more drivers in future.

But there is the support for [SauceLabs](https://saucelabs.com/ "SauceLabs") cloud service already in there. All you need is to specify credentials in environment variables (SAUCE_USER_NAME and SAUCE_API_KEY), which you can easy setup Jenkins system configuration.

## System Under Test

I decided to use [real web site](http://www.fieldaware.com) containing dynamic menu with complex structure.
But it's only for demonstrating purposes, so please, don't overload their site ;-) 

There is [test](/src/test/java/com/happytesting/menu/CheckMenuStructureTest.java "test menu structure"), which compares not only order of buttons, but all items in hover panel as well. 
All differences are reported in text form for easy understanding, what's going on, but I'm collecting screenshots as well.

Another [test](/src/test/java/com/happytesting/menu/CheckMenuLinksTest.java "test navigation") provided is navigating through menu structure and opening all links. Then it checks if new URL matches the expected one.

The menu structure is described by self descriptive [yaml file](src/test/resources/menu.yaml) for easy maintainability and to allow even testers without programming skills to update it.
After execution the report is generated, which gives good overview even for non-technical people.