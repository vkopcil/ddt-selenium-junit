package net.happytesting.fixtures.ui.fragment;

import net.happytesting.domain.menu.MenuItem;
import net.happytesting.selenium.support.TypeFactory;
import net.happytesting.selenium.support.View;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public abstract class MenuFragment implements View<MenuItem> {
  public abstract MenuItem get();

  private static final TypeFactory<MenuFragment> _FACTORY_ = new TypeFactory<MenuFragment>() {

    @Override
    public Class<? extends MenuFragment> getClass(WebElement element) {
      Class<? extends MenuFragment> menuClass = SectionFragment.class;

      if (element.findElement(By.xpath("./*")).getTagName().contentEquals("a")) {
        menuClass = LinkFragment.class;
      }
      return menuClass;
    }
  };
  
  public static TypeFactory<MenuFragment> getTypeFactory() {
    return _FACTORY_;
  }
  
//  @Deprecated
//  static MenuFragment fromElement(WebDriver driver, WebElement element) {
//    return PageFactory.createFragment(driver, element, _factory_.getClass(element));
//  }
//
//  @Deprecated
//  static Function<WebElement, MenuItem> toMenuItem(final WebDriver driver) {
//    return new Function<WebElement, MenuItem>() {
//
//      @Override
//      public MenuItem apply(WebElement item) {
//        return fromElement(driver, item).get();
//      }
//    };
//  }
}
