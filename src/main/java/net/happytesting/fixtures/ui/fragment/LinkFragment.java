package net.happytesting.fixtures.ui.fragment;

import net.happytesting.domain.menu.Link;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LinkFragment extends MenuFragment { // implements View<Link> {
  @FindBy(xpath = "./a")
  private WebElement link;
  
  @Override
  public Link get() {
    return Link.of(link.getText(), link.getAttribute("href"));
  }
}
