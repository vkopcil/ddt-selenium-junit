package net.happytesting.fixtures.ui.fragment;

import java.util.List;

import net.happytesting.domain.menu.Menu;
import net.happytesting.selenium.fragments.pagefactory.Driver;
import net.happytesting.selenium.support.Utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SectionFragment extends MenuFragment { // implements View<Menu> {
  @Driver
  private WebDriver driver;

  @FindBy(xpath = "./h4/span")
  private WebElement title;

  @FindBy(xpath = "./ul/li")
  private List<WebElement> links;

  @Override
  public Menu get() {
    Menu section = new Menu();
    section.setTitle(title.getText());
    section.setItems(Utils.fromView(driver, links, LinkFragment.class));
    return section;
  }
}
