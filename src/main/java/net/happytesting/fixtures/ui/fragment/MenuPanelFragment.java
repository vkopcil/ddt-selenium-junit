package net.happytesting.fixtures.ui.fragment;

import java.util.List;

import net.happytesting.domain.menu.Menu;
import net.happytesting.selenium.fragments.pagefactory.Driver;
import net.happytesting.selenium.support.Utils;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MenuPanelFragment {
  @Driver
  private WebDriver driver;

  @FindBy(xpath = ".")
  private WebElement activePanel;

  @FindBy(xpath = "./div[@class='row']/div")
  private List<WebElement> items;

  public Menu get() {
    Menu result = new Menu();
//    items.stream().map(item -> MenuFragment.fromElement(driver, item).get()).iterator();
    result.setItems(Utils.fromView(driver, items, MenuFragment.getTypeFactory()));
//    result.setItems(ImmutableList.copyOf(Iterables.transform(items,
//        MenuFragment.toMenuItem(driver))));
    return result;
  }

  public void openLinkFromMenu(String title) {
    WebElement link = activePanel.findElement(By.xpath(String.format(
        ".//a[. = '%s']", title)));
    Point panelLocation = activePanel.getLocation();
    Point itemLocation = link.getLocation();
    int scrollOffset = itemLocation.getY() - panelLocation.getY();
    ((JavascriptExecutor) driver).executeScript(
        "window.scrollBy(0, arguments[0]);", scrollOffset);
    link.click();
  }

}
