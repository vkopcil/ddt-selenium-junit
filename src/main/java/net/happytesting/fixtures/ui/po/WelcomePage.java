package net.happytesting.fixtures.ui.po;

import java.util.List;

import net.happytesting.fixtures.ui.fragment.MenuPanelFragment;
import net.happytesting.selenium.fragments.PageFactory;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.common.collect.ImmutableList;

public class WelcomePage {
  private final WebDriver driver;

  @FindBy(xpath = "//div[@id='main-nav']/ul/li/a")
  private List<WebElement> menuItems;

  public WelcomePage(WebDriver driver) {
    this.driver = driver;
  }

  public List<String> getMenuItems() {
    return ImmutableList.copyOf(menuItems.stream().map(item -> item.getText())
        .iterator());
  }

  public MenuPanelFragment withMouseHover(int index) {
    WebElement menu = menuItems.get(index);
    try {
      WebElement activePanel = menu
          .findElement(By
              .xpath("./following-sibling::ul[@role='menu']//div[contains(@class,'menu_right')]"));
      Dimension size = menu.getSize();
      Actions builder = new Actions(driver);
      final Action action = builder.moveToElement(menu, size.getWidth() / 2,
          size.getHeight() / 2).build();
      action.perform();
      new WebDriverWait(driver, 5).until(ExpectedConditions
          .visibilityOf(activePanel));
      return PageFactory.createFragment(driver, activePanel,
          MenuPanelFragment.class);
    } catch (NoSuchElementException e) {
      return null;
    }
  }

  public void openLinkFromTopLevelMenu(int index) {
    WebElement menu = menuItems.get(index);
    menu.click();
  }

  public void waitUntilRendered() {
    // TODO: do we need to wait for page rendering?
  }
}
