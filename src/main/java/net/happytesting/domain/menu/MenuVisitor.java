package net.happytesting.domain.menu;

public interface MenuVisitor {
  void visitLink(Link link, String path);
  void visitMenu(Menu menu, String path);
}
