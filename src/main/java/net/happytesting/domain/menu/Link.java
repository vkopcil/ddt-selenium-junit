package net.happytesting.domain.menu;


public class Link extends MenuItem {
  private String link;

  public final String getLink() {
    return link;
  }

  public final void setLink(String link) {
    this.link = link;
  }
  
  public static Link of(String title, String link) {
    Link result = new Link();
    result.setTitle(title);
    result.setLink(link);
    return result;
  }

  @Override
  public void visit(MenuVisitor visitor, String path) {
    visitor.visitLink(this, path);
  }
}
