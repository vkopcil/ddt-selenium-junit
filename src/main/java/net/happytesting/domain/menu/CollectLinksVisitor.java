package net.happytesting.domain.menu;

import java.util.LinkedList;
import java.util.List;

import net.happytesting.domain.menu.Link;
import net.happytesting.domain.menu.Menu;
import net.happytesting.domain.menu.MenuItem;
import net.happytesting.domain.menu.MenuVisitor;

public class CollectLinksVisitor implements MenuVisitor {
  public static class StoredLink {
    private String menuTitle;
    private String itemTitle;
    private String link;

    public final String getMenuTitle() {
      return menuTitle;
    }

    public final void setMenuTitle(String menuTitle) {
      this.menuTitle = menuTitle;
    }

    public final String getItemTitle() {
      return itemTitle;
    }

    public final void setItemTitle(String itemTitle) {
      this.itemTitle = itemTitle;
    }

    public final String getLink() {
      return link;
    }

    public final void setLink(String link) {
      this.link = link;
    }

    private static StoredLink of(String menuTitle, String itemTitle, String link) {
      StoredLink result = new StoredLink();
      result.setMenuTitle(menuTitle);
      result.setItemTitle(itemTitle);
      result.setLink(link);
      return result;
    }
  };
  
  private final boolean collectFullPath;
  private List<StoredLink> links = new LinkedList<>();
  
  public CollectLinksVisitor(boolean collectFullPath) {
    this.collectFullPath = collectFullPath;
  }
  
  public List<StoredLink> getLinks() {
    return links;
  }

  @Override
  public void visitMenu(Menu menu, String path) {
    if (path.isEmpty()) {
      path = menu.getTitle();
    } else if (collectFullPath) {
      path += "/" + menu.getTitle();
    }
    for (MenuItem item : ((Menu) menu).getItems()) {
      item.visit(this, path);
    }
  }
  
  @Override
  public void visitLink(Link link, String path) {
    links.add(StoredLink.of(path, link.getTitle(), link.getLink()));
  }
}
