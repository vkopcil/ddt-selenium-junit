package net.happytesting.domain.menu;

import java.util.List;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

public class Menu extends MenuItem {
  private List<MenuItem> items;

  public final List<MenuItem> getItems() {
    return items;
  }

  public final void setItems(List<MenuItem> items) {
    this.items = items;
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this).omitNullValues()
        .add("title", getTitle()).add("items", items).toString();
  }
  
  @Override
  public int hashCode() {
    return Objects.hashCode(this.items);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Menu that = (Menu) obj;
    return Objects.equal(this.items, that.items);
  }

  @Override
  public void visit(MenuVisitor visitor, String path) {
    visitor.visitMenu(this, path);
  }
}
