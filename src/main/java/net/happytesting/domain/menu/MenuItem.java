package net.happytesting.domain.menu;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

public abstract class MenuItem {
  private String title;

  public final String getTitle() {
    return title;
  }

  public final void setTitle(String title) {
    this.title = title;
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this).omitNullValues()
        .add("title", this.title).toString();
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(this.title);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    MenuItem that = (MenuItem) obj;
    return Objects.equal(this.title, that.title);
  }
  
  public abstract void visit(MenuVisitor visitor, String path);
}
