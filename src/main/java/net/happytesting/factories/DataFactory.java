package net.happytesting.factories;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import net.happytesting.domain.menu.Link;
import net.happytesting.domain.menu.Menu;
import net.happytesting.domain.menu.MenuItem;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.yaml.snakeyaml.TypeDescription;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;

public class DataFactory {
  private final static Logger LOGGER = LoggerFactory
      .getLogger(DataFactory.class);
  private final static Lock lock = new ReentrantLock();
  private final static Map<String, MenuDefinition> _INSTANCES_ = new HashMap<>();

  private DataFactory() {
  };

  public static class MenuDefinition {
    private final List<MenuItem> items;

    @SuppressWarnings("unchecked")
    public MenuDefinition(InputStream input) {
      Constructor constructor = new Constructor();
      constructor.addTypeDescription(new TypeDescription(Menu.class, "!menu"));
      constructor.addTypeDescription(new TypeDescription(Link.class, "!link"));
      Yaml yaml = new Yaml(constructor);
      items = (List<MenuItem>) yaml.load(input);
    }

    public List<MenuItem> getMenuItems() {
      return items;
    }

    public List<String> getMenuTitles() {
      return ImmutableList.copyOf(Iterables.transform(items,
          new Function<MenuItem, String>() {

            @Override
            public String apply(MenuItem menuItem) {
              return menuItem.getTitle();
            }
          }));
    }
  }

  public static final MenuDefinition withMenuDefinition(String resourcePath) {
    try {
      lock.lock();
      MenuDefinition menuDefiniton = _INSTANCES_.get(resourcePath);
      if (menuDefiniton == null) {
        menuDefiniton = existingMenuDefinition(resourcePath);
        _INSTANCES_.put(resourcePath, menuDefiniton);
      }
      return menuDefiniton;
    } finally {
      lock.unlock();
    }
  }

  private static MenuDefinition existingMenuDefinition(String resourcePath) {
    try (InputStream input = DataFactory.class
        .getResourceAsStream(resourcePath)) {
      if (input == null) {
        String message = String
            .format(
                "could not read menu definition file '%s' from resource, please check if does exist",
                resourcePath);
        LOGGER.error(message);
        throw new DataFactoryException(message);
      }
      return new MenuDefinition(input);
    } catch (IOException e) {
      throw new DataFactoryException(e);
    }
  }
}
