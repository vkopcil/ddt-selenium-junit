package net.happytesting.factories;

public class DataFactoryException extends RuntimeException {
  private static final long serialVersionUID = -6194709231703894934L;

  public DataFactoryException(String message) {
    super(message);
  }

  public DataFactoryException(String message, Throwable cause) {
    super(message, cause);
  }

  public DataFactoryException(Throwable cause) {
    super(cause);
  }
}
