package com.happytesting.domain.menu;

import static org.fest.assertions.Assertions.assertThat;

import java.util.List;

import net.happytesting.domain.menu.MenuItem;
import net.happytesting.factories.DataFactory;

import org.junit.Test;

public class DataFactoryTest {
  @Test
  public void canLoadDefinition() {
    List<MenuItem> data = DataFactory.withMenuDefinition("/menu.yaml").getMenuItems();
    assertThat(data).isNotNull();
  }
}
