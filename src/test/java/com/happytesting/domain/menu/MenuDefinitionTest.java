package com.happytesting.domain.menu;

import static org.fest.assertions.Assertions.assertThat;

import java.io.InputStream;

import net.happytesting.domain.menu.Link;
import net.happytesting.domain.menu.Menu;

import org.junit.Test;
import org.yaml.snakeyaml.TypeDescription;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;

public class MenuDefinitionTest {
  @Test
  public void canLoadDefinition() {
    InputStream input = MenuDefinitionTest.class
        .getResourceAsStream("/menu.yaml");
    Constructor constructor = new Constructor();
    constructor.addTypeDescription(new TypeDescription(Menu.class, "!menu"));
    constructor.addTypeDescription(new TypeDescription(Link.class, "!link"));
    Yaml yaml = new Yaml(constructor);
    Object data = yaml.load(input);
    assertThat(data).isNotNull();
  }
}
