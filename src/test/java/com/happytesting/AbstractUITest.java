package com.happytesting;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.happytesting.factories.DriverFactory;
import com.happytesting.fixtures.ui.StepDefinitionsUI;

public abstract class AbstractUITest {
  private static final Logger LOGGER = LoggerFactory
      .getLogger(AbstractUITest.class);
  public static final String PLATFORM_URL = "http://www.fieldaware.com/";
  public static final String MENU_DEFINITION = "/menu.yaml";

  private static final Lock lock = new ReentrantLock();
  private static final Map<Thread, WebDriver> allDrivers = new HashMap<>();
  
  private StepDefinitionsUI steps;
  
  protected StepDefinitionsUI getSteps() {
    return steps;
  }

  @Before
  public void reset() throws Throwable {
    boolean doSetup = false;
    WebDriver currentDriver;
    try {
      lock.lock();
      currentDriver = allDrivers.get(Thread.currentThread());
      if (currentDriver == null) {
        doSetup = true;
        currentDriver = DriverFactory.createDriver();
        allDrivers.put(Thread.currentThread(), currentDriver);
      }
    } finally {
      lock.unlock();
    }
    steps = new StepDefinitionsUI(currentDriver); 
    steps.setPlaformURL(PLATFORM_URL);
    steps.existingMenuDefinition(MENU_DEFINITION);
    if (doSetup || !steps.getCurrentPageUrl().contentEquals(PLATFORM_URL)) {
      steps.visitTheSite();
    }
  }

  @After
  public void cleanupThread() {
    steps.afterScenarioCleanup();
  }

  @AfterClass
  public static void tearDown() {
    LOGGER.info("releasing driver instances");
    try {
      lock.lock();
      for (int index = allDrivers.size(); index > 0; index--) {
        Thread thread = allDrivers.keySet().iterator().next();
        WebDriver driver = allDrivers.remove(thread);
        DriverFactory.releaseDriver(driver);
      }
    } finally {
      lock.unlock();
    }
  }
}
