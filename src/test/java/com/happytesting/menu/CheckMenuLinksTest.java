package com.happytesting.menu;

import static org.fest.assertions.Assertions.assertThat;

import java.io.IOException;

import net.happytesting.domain.menu.CollectLinksVisitor;
import net.happytesting.domain.menu.MenuItem;
import net.happytesting.domain.menu.CollectLinksVisitor.StoredLink;
import net.happytesting.factories.DataFactory;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import ru.yandex.qatools.allure.annotations.Step;

import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.happytesting.AbstractUITest;

@RunWith(Parameterized.class)
public class CheckMenuLinksTest extends AbstractUITest {
  private final String menuTitle;
  private final String itemTitle;
  private final String link;

  public CheckMenuLinksTest(String menuTitle, String itemTitle, String link)
      throws IOException {
    this.menuTitle = menuTitle;
    this.itemTitle = itemTitle;
    this.link = link;
  }

  @Test
  public void canOpenLinkFromMenu() throws Throwable {
    if (itemTitle == null || itemTitle.length() == 0) {
      getSteps().openLinkFromMenu(menuTitle);
    } else {
      getSteps().openLinkFromMenu(menuTitle, itemTitle);
    }
    checkPageUrl();
  }
  
  @Step
  private void checkPageUrl() {
    String url = getSteps().getCurrentPageUrl();
    if (url.startsWith(PLATFORM_URL)) {
      url = url.substring(PLATFORM_URL.replaceFirst("/$", "").length());
    }
    assertThat(url).as("page URL").isEqualTo(link);
  }
  
  @Parameters(name = "{0}/{1}")
  public static Iterable<Object[]> data() {
    CollectLinksVisitor collector = new CollectLinksVisitor(false);
    for (MenuItem subMenu : DataFactory.withMenuDefinition(MENU_DEFINITION).getMenuItems()) {
      subMenu.visit(collector, "");
    }
    Iterable<Object[]> rows = Iterables.transform(collector.getLinks(), new Function<StoredLink, Object[]> () {

      @Override
      public Object[] apply(StoredLink link) {
        if (link.getMenuTitle().isEmpty()) {
          return new Object[] { link.getItemTitle(), "", link.getLink()};
        }
        return new Object[] { link.getMenuTitle(), link.getItemTitle(), link.getLink()};
      }});
    
    return rows;
  }

}
