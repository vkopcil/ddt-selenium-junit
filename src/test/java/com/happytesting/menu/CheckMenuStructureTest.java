package com.happytesting.menu;

import static org.fest.assertions.Assertions.assertThat;

import java.io.IOException;

import net.happytesting.domain.menu.Menu;
import net.happytesting.domain.menu.MenuItem;
import net.happytesting.factories.DataFactory;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.happytesting.AbstractUITest;

@RunWith(Parameterized.class)
public class CheckMenuStructureTest extends AbstractUITest {
  private final String menuTitle;
  private final MenuItem menu;

  public CheckMenuStructureTest(String menuTitle, MenuItem menu)
      throws IOException {
    this.menuTitle = menuTitle;
    this.menu = menu;
  }

  @Test
  public void barButtonsAreDisplayed() throws Throwable {
    getSteps().barButtonsAreDisplayed();
  }

  @Test
  public void hoverOverButton() {
    MenuItem activeMenu = getSteps().hoverOverButton(menuTitle);
    if (activeMenu != null) {
      assertThat(((Menu) activeMenu).getItems()).isEqualTo(
          ((Menu) this.menu).getItems());
    }
  }

  @Parameters(name = "{0}")
  public static Iterable<Object[]> data() {
    return Iterables.transform(DataFactory.withMenuDefinition(MENU_DEFINITION)
        .getMenuItems(), new Function<MenuItem, Object[]>() {

      @Override
      public Object[] apply(MenuItem menuItem) {
        return new Object[] { menuItem.getTitle(), menuItem };
      }
    });
  }
}
