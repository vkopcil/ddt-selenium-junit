package com.happytesting.factories;

public class DriverFactoryException extends RuntimeException {
  private static final long serialVersionUID = 5193547554345377387L;

  public DriverFactoryException(String message) {
    super(message);
  }

  public DriverFactoryException(String message, Throwable cause) {
    super(message, cause);
  }

  public DriverFactoryException(Throwable cause) {
    super(cause);
  }
}
