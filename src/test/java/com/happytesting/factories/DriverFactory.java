package com.happytesting.factories;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.saucelabs.common.SauceOnDemandAuthentication;

public class DriverFactory {
  private static final Logger LOGGER = LoggerFactory
      .getLogger(DriverFactory.class);
  private static final String DEFAULT_BROWSER = "firefox";
  private static final Browser BROWSER;

  private DriverFactory() {
  }

  static {
    String browser = System.getProperty("BROWSER");
    if (browser == null) {
      LOGGER.warn("BROWSER property is not specified, defaulting to "
          + DEFAULT_BROWSER);
      browser = DEFAULT_BROWSER;
    }
    BROWSER = Browser.byName(browser);
  }

  public static void releaseDriver(WebDriver driver) {
    if (driver != null) {
      LOGGER.info("releasing driver instance");
      driver.quit();
    }
  }

  private static final SauceOnDemandAuthentication authentication = new SauceOnDemandAuthentication();

  public static WebDriver createDriver() {
    LOGGER.info("creating driver instance");
    WebDriver driver = null;
    if (authentication.getUsername() != null
        && authentication.getAccessKey() != null) {
      DesiredCapabilities capabilities = new DesiredCapabilities(
          BROWSER.getName(), "", Platform.WINDOWS);
      // Create the connection to Sauce Labs to run the tests
      try {
        driver = new RemoteWebDriver(new URL("http://"
            + authentication.getUsername() + ":"
            + authentication.getAccessKey()
            + "@ondemand.saucelabs.com:80/wd/hub"), capabilities);
      } catch (MalformedURLException e) {
        throw new DriverFactoryException(e);
      }
    } else {
      driver = BROWSER.create();
    }
    return driver;
  }
}