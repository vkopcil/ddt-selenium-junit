package com.happytesting.factories;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.BrowserType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public enum Browser {
  FIREFOX(BrowserType.FIREFOX) {
    @Override
    protected WebDriver create() {
      return new FirefoxDriver();
    }
  },
  CHROME(BrowserType.CHROME), IE(BrowserType.IEXPLORE);
  // DesiredCapabilities caps = new DesiredCapabilities();
  // caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,
  // "/Path/to/bin/phantomjs");
  // driver = new PhantomJSDriver();

  private static final Logger LOGGER = LoggerFactory.getLogger(Browser.class);
  private String name;

  private Browser(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  public static Browser byName(String name) {
    for (Browser browser: values()) {
      if (browser.getName().contentEquals(name)) {
        return browser;
      }
    }
    throw new IllegalArgumentException(String.format("No enum constant %s for name %s", Browser.class.getName(), name));
  }

  protected WebDriver create() {
    String message = String.format(
        "browser %s is not supported for local execution (yet)", getName());
    LOGGER.error(message);
    throw new DriverFactoryException(message);
  }
}
