package com.happytesting.fixtures.ui;

import static org.fest.assertions.Assertions.assertThat;

import java.util.List;

import net.happytesting.domain.menu.Menu;
import net.happytesting.factories.DataFactory;
import net.happytesting.fixtures.ui.fragment.MenuPanelFragment;
import net.happytesting.fixtures.ui.po.WelcomePage;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ru.yandex.qatools.allure.annotations.Attachment;
import ru.yandex.qatools.allure.annotations.Step;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;

public class StepDefinitionsUI {
  private final static Logger LOGGER = LoggerFactory
      .getLogger(StepDefinitionsUI.class);
  private final WebDriver driver;
  private final String originalWindow;

  private String platformUrl;
  private String menuDefinition;
  private final WelcomePage welcomePage;

  public StepDefinitionsUI(WebDriver driver) {
    this.driver = driver;
    originalWindow = driver.getWindowHandle();
    resizeBrowserWindow();
    welcomePage = PageFactory.initElements(driver, WelcomePage.class);
  }
  
  // @Given("^Plaform URL \"(.*?)\"$")
  public void setPlaformURL(String url) {
    this.platformUrl = url;
  }

  // @Given("existing menu definition \"(.*)\"")
  public void existingMenuDefinition(String menuDefinition) {
    this.menuDefinition = menuDefinition;
  }

  // @When("I visit the site")
  @Step
  public void visitTheSite() throws Throwable {
    LOGGER.info("navigating to " + this.platformUrl);
    driver.get(this.platformUrl);
    welcomePage.waitUntilRendered();
  }

  // @Then("bar buttons are displayed")
  @Step
  public void barButtonsAreDisplayed() {
    List<String> expected = DataFactory.withMenuDefinition(menuDefinition).getMenuTitles();
    List<String> actual = welcomePage.getMenuItems();
    assertThat(actual).as("current menu items").isEqualTo(expected);
    // .waitUntilIsNotDisplayed();
  }

  @Step
  private int findMenuByTitle(final String expectedTitle) {
    List<String> actual = welcomePage.getMenuItems();
    int index = Iterables.indexOf(actual, new Predicate<String>() {

      @Override
      public boolean apply(String title) {
        return title.contentEquals(expectedTitle);
      }
    });
    if (index < 0) {
      throw new RuntimeException(String.format("could find menu item '%s'",
          expectedTitle));
    }
    return index;
  }

  @Step
  public Menu hoverOverButton(final String expectedTitle) {
    int index = findMenuByTitle(expectedTitle);
    MenuPanelFragment menu = welcomePage.withMouseHover(index);
    return menu != null? menu.get(): null;
  }

  @Step
  public void openLinkFromMenu(final String menuTitle, final String itemTitle) {
    int index = findMenuByTitle(menuTitle);
    welcomePage.withMouseHover(index).openLinkFromMenu(itemTitle);
  }

  @Step
  public void openLinkFromMenu(final String menuTitle) {
    int index = findMenuByTitle(menuTitle);
    welcomePage.openLinkFromTopLevelMenu(index);
  }
  
  @Step
  public String getCurrentPageUrl() {
    return driver.getCurrentUrl();
  }

  // @Then("^(?:I can see )?page title \"(.*?)\"$")
  // @Step("I can see page title")
  // public void validateTitle(String expectedTitle) throws Throwable {
  // assertThat(welcomePage.getTitle()).as("title").isEqualTo(expectedTitle);
  // }

  public void afterScenarioCleanup() {
    if (driver != null) {
      // TODO: close any alerts and extra windows

      // capture the dom
      captureDom(driver);

      // capture a screenshot
      captureScreenshot(driver);

      while (!driver.getWindowHandle().contentEquals(originalWindow)) {
        driver.close();
        driver.switchTo().window(Iterables.getLast(driver.getWindowHandles()));
      }

      driver.manage().deleteAllCookies();
      resizeBrowserWindow();
    }
  }

  private void resizeBrowserWindow() {
    driver.manage().window().setSize(new Dimension(1024, 768));
    driver.manage().window().setPosition(new Point(0, 0));
  }

  @Attachment(value = "captured screenshot")
  public byte[] captureScreenshot(WebDriver driver) {
    if (!(driver instanceof TakesScreenshot)) {
      return null;
    }
    byte[] capturePage = ((TakesScreenshot) driver)
        .getScreenshotAs(OutputType.BYTES);
    return capturePage;
  }

  @Attachment(value = "captured DOM")
  public String captureDom(WebDriver driver) {
    String capturedDom = driver.getPageSource();
    return capturedDom;
  }
}